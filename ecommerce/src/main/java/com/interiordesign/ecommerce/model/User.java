package com.interiordesign.ecommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.*;

@Data
@Entity
@Table(name="users",  uniqueConstraints = {
        @UniqueConstraint(columnNames = "username"),
        @UniqueConstraint(columnNames = "email")
})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private Long id;

    @NotBlank @Size(max=20)
    @Column(name="username", nullable = false, unique = true)
    private String username;

    @NotBlank @Size(max=50)
    @Column(name="email", nullable = false, unique = true)
    @Email(message = "Email must be valid")
    private String email;

    @NotBlank @Size(max=120) @Length(min=8,message = "Password must have at least 8 characters!")
    @Column(name="password", nullable = false)
    private String password;

    @Pattern(regexp = "^\\d{10}$", flags = Pattern.Flag.UNICODE_CASE, message = "Phone number must have 10 numbers!")
    @Column(name="phone_number")
    private String phoneNumber;

    @Column(name="registration_date")
    @NotBlank @CreationTimestamp
    private Date registrationDate;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Order> orders = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Cart> carts = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Address> addresses = new HashSet<>();
}
