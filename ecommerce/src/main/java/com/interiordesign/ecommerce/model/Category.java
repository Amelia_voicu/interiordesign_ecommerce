package com.interiordesign.ecommerce.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name="categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="category_name", nullable = false,
            unique = true, length = 50)
    private String categoryName;

    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Project> projects=new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Category> categories = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="parent_id")
    private Category category;

    @Override
    public String toString() {
        return  " Category id: " + id +'\'' +
                ", categoryName: '" + categoryName + '\'' +
                ", parent_id: " + (category!=null ?
                category.getId():"");
    }
}
