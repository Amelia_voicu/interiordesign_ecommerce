package com.interiordesign.ecommerce.model;

public enum RoleType {
    ROLE_USER,
    ROLE_ADMIN
}
