package com.interiordesign.ecommerce.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity(name="packages")
public class Package {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="package_id")
    private long packageId;

    @NotBlank
    @Column(name="package_name", nullable = false)
    private String packageName;

    @Column(name="description", columnDefinition = "TEXT")
    private String description;

    @Column(name="price")
    private BigDecimal price;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "stock")
    private int stock;

    @CreationTimestamp
    @Column(name="created_At", nullable = false)
    private Date createdAt;

    @ManyToMany(mappedBy = "packages", cascade = CascadeType.PERSIST)
    Set<Cart> carts = new HashSet<>();

    @ManyToMany(mappedBy = "packages", cascade = CascadeType.PERSIST)
    Set<Order> orders = new HashSet<>();

}
