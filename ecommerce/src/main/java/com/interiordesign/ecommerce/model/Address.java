package com.interiordesign.ecommerce.model;

import com.sun.istack.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cart_id")
    private long addressId;

    @NotBlank @Size(max=30)
    @Column(name="city", nullable = false)
    private String city;

    @NotBlank @Size(max=30)
    @Column(name="region", nullable = false)
    private String region;

    @Column(name="full_address")
    private String fullAddress;

    @ManyToOne
    @NotNull
    private User user;

}
