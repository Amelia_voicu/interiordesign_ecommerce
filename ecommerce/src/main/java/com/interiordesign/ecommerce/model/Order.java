package com.interiordesign.ecommerce.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity(name="orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="order_id")
    private long orderId;

    @NotBlank @CreationTimestamp
    @Column(name="created_At", nullable = false)
    private Date createdAt;

    @NotBlank
    @Column(name="order_total_amount", nullable = false)
    private double orderTotalAmount;

    @NotBlank
    @Column(name="order_quantity", nullable = false)
    private int orderQuantity;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "order_package",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "package_id")
    )
    private Set<Package> packages = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="address_id")
    private Address address;

}
