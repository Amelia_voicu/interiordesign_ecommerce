package com.interiordesign.ecommerce.model;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="project_id")
    private long projectId;

    @Column(name="project_name", nullable = false)
    private String projectName;

    @Column(name="description", columnDefinition = "TEXT")
    private String description;

    @Column(name="sqm")
    private Double sqm;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name="date_added")
    @CreationTimestamp
    private Date dateAdded;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
